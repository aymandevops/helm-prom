provider "google" {
  project     = "atomic-kit-236110"
  region      = "us-central1-c"
}
provider "kubernetes" {
  config_path    = "~/.kube/config"
  #config_context = "my-context"
}

provider "helm" {
  kubernetes {
    config_path = "~/.kube/config"
  }
}

## Create namespace
resource "kubernetes_namespace" "nginx-ingress" {
  metadata {
    name = "nginx-ingress"
  }
}
resource "kubernetes_namespace" "monitoring" {
  metadata {
    name = "monitoring"
  }
}


##Deploy Nginx Ingress Controller

resource "helm_release" "nginx-ingress" {
  name       = "nginx-ingress"
  chart      = "../ingress-nginx"
  namespace  = "nginx-ingress"
  depends_on = [kubernetes_namespace.nginx-ingress]
}

resource "helm_release" "kube-state-metrics" {
  name       = "kube-state-metrics"
  chart      = "../kube-state-metrics"
  namespace  = "monitoring"
  depends_on = [kubernetes_namespace.monitoring]
}


resource "helm_release" "prometheus" {
  name       = "prometheus"
  chart      = "../prometheus"
  namespace = "monitoring"
  depends_on = [kubernetes_namespace.monitoring]
}
resource "helm_release" "grafana" {
  name       = "grafana"
  chart      = "../grafana"
  namespace = "monitoring"
  depends_on = [kubernetes_namespace.monitoring]
}
